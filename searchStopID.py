import argparse
import requests
from tabulate import tabulate

parser = argparse.ArgumentParser(description="Look up stop name and ID using text search.")
parser.add_argument('input', help="Location to search for.", action='store')
parser.add_argument('-d', '--debug', help="Show API link. Default: off", action='store_true')
args = parser.parse_args()

otable = []

dataurl = requests.get('http://xmlopen.rejseplanen.dk/bin/rest.exe/location?format=json&input=%s' % args.input)
datajson = dataurl.json()
stoploc = datajson['LocationList']['StopLocation']

htable = ["Name", "Stop ID"]

if args.debug is True:
    print('http://xmlopen.rejseplanen.dk/bin/rest.exe/location?format=json&input=%s' % args.input)

for stoploc in datajson['LocationList']['StopLocation']:
    otable.append([stoploc["name"], stoploc["id"]])

print(tabulate(otable, headers=htable))

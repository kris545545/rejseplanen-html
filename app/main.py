import requests
import time
from flask import Flask, render_template, request

# Set time and date

# sample stations
# Århus H
Ar = '8600053'
# København H
Kh = '8600626'
# Fredericia
Fa = '8600079'
# Odense
Od = '8600512'
# Struer
Str = '8600189'
# Gunnar Clausens Vej
# Hasselager Allé
Gct = '751422803'
# Dybbølsbro
Dbt = '008600634'
# Køge
Kj = '008600803'
# Viby J
Vi = '008600054'

# Define station
# Use ../searchStopID.py to get the ID based on the name
stopid = Dbt

# Show/hide buses
usebus = '0'

# Get station name
stoptext = requests.get('http://xmlopen.rejseplanen.dk/bin/rest.exe/location?input=%s&format=json' % (stopid))
stopname = stoptext.json()['LocationList']['StopLocation']['name']

# Get data
def getData():
    urltime = time.strftime('%H:%M')
    urldate = time.strftime('%d.%m.%y')
    dataurl = requests.get('http://xmlopen.rejseplanen.dk/bin/rest.exe/departureBoard?format=json&id=%s&date=%s&time=%s&useBus=%s' % (stopid, urldate, urltime, usebus))
    #dataurl = requests.get('http://127.0.0.1/static/sample.json')
    data = dataurl.json()
    dep = data['DepartureBoard']['Departure']
    print(f"Fetching data - http://xmlopen.rejseplanen.dk/bin/rest.exe/departureBoard?format=json&id={stopid}&date={urldate}&time={urltime}&useBus={usebus}")
    return dep

# Webserver stuff
app = Flask(__name__)
@app.route('/')

def template():
    return render_template("index.jinja", title=stopname, deps=getData())

app.run(debug=True, host='0.0.0.0')
